<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{   
    protected $table = 'staff';
    protected $fillable = ['id','fullname','address','tel']; 
    public function owners() {
    return $this->hasMany(Owner::class);
   }
    public function users(){
        return $this->belongsTo(User::class);
    }
}
