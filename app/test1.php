<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
class Test1 extends Authenticatable
{
    use Notifiable;
    
    protected  $guard = "admin";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'owner';
    protected $fillable = ['id','email','fullname','password','address','tel','staff_id','stock_id'];
    public  $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function owners() {
    return $this->hasOne(Owner::class,'id'); 
    }
    
    public function staffs() {
    return $this->belongsTo(Staff::class,'id'); 
    }
    public function stocks() {
    return $this->belongsTo(Stock::class,'id');
    } 
    
}
