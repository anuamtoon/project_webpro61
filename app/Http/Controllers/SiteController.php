<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index(){
        $fullname = "Oat Hansome";
        $website = "codingthailan.com";
        return view('about',['fullname' =>$fullname,
            'website' => $website
            ]);
    }
}
