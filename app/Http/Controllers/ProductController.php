<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Http\Requests\StoreProductsRequest;
use Image;
use File;
use DB;
class ProductController extends Controller
{
     public function __construct() {
     $this->middleware('auth', ['except' => ['index']]);
    //$this->middleware('auth', ['except' => ['index', 'create', 'store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $products = Product::all();
        $products = Product::orderBy('id','desc')->get();
        $count = Product::count(); //นับจํานวนแถวทั งหมด

        //แบ่งหน้า
        //$typebooks = TypeBooks::simplePaginate(3);
        $products = Product::paginate(3);

        return view('products.index', [
        'products' => $products,
        'count' => $count
        ]); 
    }
    
    public function search(Request $requset){
        $searchData = $requset->searchData;
//return $request->all();
//        $search = $request->get('search');
        $product = DB::table('product')->where('fullname','like','%'.$searchData.'%')
                ->get();
     //   echo $product;
       return view('products.Show', [
        'products' => $product,
        '' => $searchData
        ]); 
       // return view('products.Show',['product'=>$product, '' => $searchData]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('products.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductsRequest $request)
    {
        $products = new Product();
        $products->fullname = $request->fullname;
        $products->info = $request->info;
        $products->price = $request->price;
        $products->type = $request->type;
        if ($request->hasFile('image')) {
        $filename = str_random(10) . '.' . $request->file('image')->getClientOriginalExtension(); 
        $request->file('image')->move(public_path() . '/images/', $filename);
        Image::make(public_path() . '/images/' . $filename)->resize(50, 50)->save(public_path() . '/images/resize/' .
       $filename);
        $products->image = $filename;
        } else {
        $products->image = 'nopic.jpg';
        }
        $products->save();
        
        $request->session()->flash('status', 'บันทึกข้อมูลเรียบร้อยแล้ว');//กําหนด key ของ flash data ชือว่าstatus โดยใส่ค่าข้อมูลคําว่า บันทึกข้อมูลเรียบร้อยแล้ว
        return redirect()->action('ProductController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $products = Product::findOrFail($id);
       return view('products.edit', ['product' => $products]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProductsRequest $request, $id)
    {
      
        $products = Product::find($id);
        $products->fullname = $request->fullname;
        $products->info = $request->info;
        $products->price = $request->price;
        $products->type = $request->type;
        if ($request->hasFile('image')) {
        // delete old file before update
        if ($products->image != 'nopic.jpg') {
        File::delete(public_path() . '\\images\\' . $products->image);
        File::delete(public_path() . '\\images\\resize\\' . $products->image);
        }
        $filename = str_random(10) . '.' . $request->file('image')->getClientOriginalExtension();
        $request->file('image')->move(public_path() . '/images/', $filename);
        Image::make(public_path() . '/images/' . $filename)->resize(50, 50)->save(public_path() . '/images/resize/' .
       $filename);
        $products->image = $filename;
        }
        $products->save();
       
        return redirect()->action('ProductController@index');
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $products = Product::find($id);
        if ($products->image != 'nopic.jpg') {
        File::delete(public_path() . '\\images\\' . $products->image);
        File::delete(public_path() . '\\images\\resize\\' . $products->image);
        }
        $products->delete();
        return redirect()->action('ProductController@index');
    }
   
    
}
