<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;
use App\Http\Requests\StoreStaffRequest;
use Image;
use File;
class StaffController extends Controller
{
     public function __construct() {
     $this->middleware('auth', ['except' => ['index']]);
    //$this->middleware('auth', ['except' => ['index', 'create', 'store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $staffs = Staff::all();
        $staffs = Staff::orderBy('id','desc')->get();
        $count = Staff::count(); //นับจํานวนแถวทั งหมด

        //แบ่งหน้า
        //$typebooks = TypeBooks::simplePaginate(3);
        $staffs = Staff::paginate(3);

        return view('staffs.index', [
        'staffs' => $staffs,
        'count' => $count
        ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('staffs.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStaffRequest $request)
    {
        $staffs = new Staff();
        $staffs->fullname = $request->fullname;
        $staffs->address = $request->address;
        $staffs->tel = $request->tel;
        $staffs->save();
        
        $request->session()->flash('status', 'บันทึกข้อมูลเรียบร้อยแล้ว');//กําหนด key ของ flash data ชือว่าstatus โดยใส่ค่าข้อมูลคําว่า บันทึกข้อมูลเรียบร้อยแล้ว
        return redirect()->action('StaffController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $staffs = Staff::findOrFail($id);
       return view('staffs.edit', ['staff' => $staffs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreStaffRequest $request, $id)
    {
      
        $staffs = Staff::find($id);
         $staffs->fullname = $request->fullname;
        $staffs->address = $request->address;
        $staffs->tel = $request->tel;
        $staffs->save();
       
       return redirect()->action('StaffController@index');
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
    {
        $staffs = Staff::find($id)->delete();
       
        return redirect()->action('StaffController@index');
    }
   
    
}
