<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Owner;
use App\User;
use Auth;
use App\Http\Requests\StoreOwnerRequest;
use Image;
use File;
class OwnerController extends Controller
{
     public function __construct() {
        
     $this->middleware('auth');
    //$this->middleware('auth', ['except' => ['index', 'create', 'store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
       // return "osasdasd";
       // $owners = Owner::all();
      
        $owners = Owner::with('stocks')->orderBy('id', 'desc')->paginate(5);
        $owners = Owner::with('staffs')->orderBy('id', 'desc')->paginate(5);
        // $owners = Owner::orderBy('id','desc')->get();
        $count = Owner::count(); //นับจํานวนแถวทั งหมด

        //แบ่งหน้า
        //$typebooks = TypeBooks::simplePaginate(3);
       // $owner = Owner::paginate(3);

        return view('owners.index', [
        'owners' => $owners,
        'count' => $count
        ]); 
     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('owners.create');

    }
    
     public function search(Request $request){
         $search = $request->get();
         $posts = DB::table('owners')->where('fullname','status','%'.$search.'%')->paginate(5);
         return view('index',['owner' => $posts]);
         
     }
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOwnerRequest $request)
    {
        
        
        $owners = new Owner();
        $owners->fullname = $request->fullname;
        $owners->address = $request->address;
        $owners->tel = $request->tel;
        $owners->status = $request->status;
        $owners->staff_id = $request->staff_id;
        $owners->stock_id = $request->stock_id;
       
        $owners->save();
        
        $request->session()->flash('status', 'บันทึกข้อมูลเรียบร้อยแล้ว');//กําหนด key ของ flash data ชือว่าstatus โดยใส่ค่าข้อมูลคําว่า บันทึกข้อมูลเรียบร้อยแล้ว
        return redirect()->action('OwnerController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function show($id)
    {
        //
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    
       $users = User::with('owners')->find($id);
       return view('owners.edit', ['users' => $users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $request, $id=null)
    {
        $owners = Owner::find($id);
        $owners->fullname = $request->fullname;
        $owners->address = $request->address;
        $owners->tel = $request->tel;
        $owners->status = $request->status;
        $owners->staff_id = $request->staff_id;
        $owners->stock_id = $request->stock_id;
       
        $owners->save();
        
        return redirect()->action('OwnerController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $owners = Owner::find($id);
        $owners->delete();
       return redirect()->action('OwnerController@index');
    }
   
    
}
