<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Http\Requests\StoreStockRequest;
use Image;
use File;
class StockController extends Controller
{
     public function __construct() {
     $this->middleware('auth', ['except' => ['index']]);
    //$this->middleware('auth', ['except' => ['index', 'create', 'store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $stocks = Stock::with('products')->orderBy('id', 'desc')->paginate(5);
         
        
        /*$stocks = Stock::all();
        $stocks = Stock::orderBy('id','desc')->get();*/
        $count = Stock::count(); //นับจํานวนแถวทั งหมด

        //แบ่งหน้า
        //$typebooks = TypeBooks::simplePaginate(3);
        

        return view('stocks.index', [
        'stocks' => $stocks,
        'count' => $count
        ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('stocks.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStockRequest $request)
    {
        $stocks = new Stock();
        $stocks->fullname = $request->fullname;
        $stocks->address = $request->address;
        $stocks->tel = $request->tel;
        $stocks->product_id = $request->product_id;
        
      
        $stocks->save();
        
        $request->session()->flash('status', 'บันทึกข้อมูลเรียบร้อยแล้ว');//กําหนด key ของ flash data ชือว่าstatus โดยใส่ค่าข้อมูลคําว่า บันทึกข้อมูลเรียบร้อยแล้ว
        return redirect()->action('StockController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $stocks = Stock::findOrFail($id);
       return view('stocks.edit', ['stocks' => $stocks]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreStockRequest $request, $id)
    {
      
        $stocks = Stock::find($id);
       $stocks->fullname = $request->fullname;
        $stocks->address = $request->address;
        $stocks->tel = $request->tel;
        $stocks->product_id = $request->product_id;
        $stocks->save();
         return redirect()->action('StockController@index');
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stocks = Stock::find($id);
        $stocks->delete();
        return redirect()->action('StockController@index');
    }
   
    
}
