<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreStockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        //return Auth::check(); 
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'fullname' => 'required',
            'address' => 'required',
            'tel' => 'required',
            'product_id' => 'required',
            
           
        ];
    }
     public function messages() {
        return [
        'fullname.required' => 'กรุณากรอกชื่อ',
        'address.required' => 'กรุณากรอกที่อยู่',
        'tel.required' => 'กรุณากรอกเบอร์โทร',
        'product_id.required' => 'กรุณากรอกรหัสสินค้า',
        
        
        ];
        }
}
