<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        //return Auth::check(); 
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname' => 'required',
            'info' => 'required',
            'price' => 'required',
            'type' => 'required',
            'image' => 'mimes:jpeg,jpg,png',
        ];
    }
     public function messages() {
        return [
        'fullname.required' => 'กรุณากรอกชือสินค้า',
        'info.required' => 'กรุณากรอกรายละเอียดสินค้า',
        'price.required' => 'กรุณากรอกราคา',
        'type.required' => 'กรุณากรอกชนิดสินค้า',
        'image.mimes' => 'กรุณาเลือกไฟล์ภาพนามสกุล jpeg,jpg,png',
        ];
        }
}
