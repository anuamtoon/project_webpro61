<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOwnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        //return Auth::check(); 
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'fullname' => 'required',
            'address' => 'required',
            'tel' => 'required',
            'status' => 'required',
            'staff_id' => 'required',
            'stock_id' => 'required',
           
        ];
    }
     public function messages() {
        return [
        'fullname.required' => 'กรุณากรอกชื่อ',
        'address.required' => 'กรุณากรอกที่อยู่',
        'tel.required' => 'กรุณากรอกเบอร์โทร',
        'status.required' => 'กรุณากรอกสถานะ',
        'staff_id.required' => 'กรุณากรอกรหัสพนักงาน',
        'stock_id.required' => 'กรุณากรอกรหัสสต็อก',
        
        ];
        }
}
