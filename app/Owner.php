<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    protected $table = 'owners';
    protected $fillable = ['id','fullname','address','tel','status','staff_id','stock_id'];
   
   
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function staffs() {
    return $this->belongsTo(Staff::class,'id'); 
    }
    public function stocks() {
    return $this->belongsTo(Stock::class,'id');
    } 
     
    
}
