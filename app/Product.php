<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
   protected $table = 'product';
    protected $fillable = ['id','fullname','type','info','price','image'];
    
  public function stocks() {
  return $this->hasMany(Stock::class);
  }
}
