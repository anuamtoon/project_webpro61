<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'stock';
    protected $fillable = ['id','fullname','address','tel','product_id'];
    
     
     public function products(){
        return $this->belongsTo(Product::class,'id');
    }
    
    public function owners(){
        return $this->belongsTo(Owner::class);
    }
    
}
