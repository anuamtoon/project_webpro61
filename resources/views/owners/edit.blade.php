@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row">
 <div class="col-lg-10 col-log-offset-1">
 <div class="card mt-3">
 <div class="card-header h3">
 แก้ไขข้อมูล {{ $users-> name }}
 </div>
 <div class="card-body">
 @if (count($errors) > 0)
<div class="alert alert-warning">
 <ul>
 @foreach ($errors->all() as $error)
<li>{{ $error }}</li>
 @endforeach
 </ul>
 </div>
@endif
 <?= Form::model($users, array('url' => 'owners/' . $users->id,
'method' => 'put','files' => true)) ?>
<div class="form-group">
 <?= Form::label('name', 'Username');
?>
 <?= Form::text('name',isset($users->name)? $users->name: null, ['class' =>
'form-control', 'placeholder' => 'Username']);
?>
 </div>
<div class="form-group">
 {!! Form::label('email', 'อีเมล'); !!}
{!! Form::text('email',isset($users->email)? $users->email: null,['class' =>'form-control','placeholder' => 'name@gmail.com']); !!}
 </div>
<div class="form-group">
 <?= Form::label('fullname', 'ชื่อ-นามสกุล');
?>
 <?= Form::text('fullname' ,isset($users->owners->fullname)? $users->owners->fullname:null, ['class' =>
'form-control', 'placeholder' => 'ชื่อ-นามสกุล']);
?>
 </div>
 
  <div class="form-group">
 <?= Form::label('address', 'ที่อยู่');
?>
 <?= Form::text('address',isset($users->owners->address)? $users->owners->address:null, ['class' =>
'form-control', 'placeholder' => 'ที่อยู่']);
?>
 </div>
 <div class="form-group">
 <?= Form::label('tel', 'โทรศัทพ์');
?>
 <?= Form::text('tel',isset($users->owners->tel)? $users->owners->tel:null, ['class' =>
'form-control', 'placeholder' => 'โทรศัทพ์']);
?>
 </div>
<div class="form-group">
 <?= Form::label('staff_id', 'รหัสพนักงาน');
?>
 <?= Form::text('staff_id', isset($users->owners->staff_id)? $users->owners->staff_id:null, ['class' =>
'form-control', 'placeholder' => 'รหัสพนักงาน']);
?>
 </div>
       <div class="form-group">
 <?= Form::label('stock_id', 'รหัสสต็อก');
?>
 <?= Form::text('stock_id', isset($users->owners->stock_id)? $users->owners->stock_id:null, ['class' =>
'form-control', 'placeholder' => 'รหัสสต็อก']);
?>
 </div>
     <div class="form-group">
 <?= Form::label('status', 'สถานนะ');
?>
 <?= Form::text('status', isset($users->owners->status)? $users->owners->status:null, ['class' =>
'form-control', 'placeholder' => 'สถานนะ']);
?>
 </div>
 <div class="form-group">
 <?= Form::submit('แก้ไขข้อมูล', ['class' => 'btn btn-primary']); ?>
 </div>
 {!! Form::close() !!}
 </div>
 </div>
 </div>
 </div>
</div>
@endsection