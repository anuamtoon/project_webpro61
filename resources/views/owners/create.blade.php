@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row">
 <div class="col-lg-10 col-log-offset-1">
 <div class="card mt-3">
 <div class="card-header h3">
 เพิม User เจ้าของกิจการ
 </div>
     
     
 @if (count($errors) > 0)
 <div class="alert alert-warning">
 <ul>
 @foreach ($errors->all() as $error)
 <li>{{ $error }}</li>
 @endforeach
 </ul>
 </div>
 @endif
 
 
  <div class="card-body">
  {!! Form::open(array('url' => 'owners','files' =>
true)) !!}
<div class="form-group">
 <?= Form::label('name', 'Username');
?>
 <?= Form::text('name', null, ['class' =>
'form-control', 'placeholder' => 'Username']);
?>
 </div>
<div class="form-group">
 <?= Form::label('fullname', 'ชื่อ-นามสกุล');
?>
 <?= Form::text('fullname', null, ['class' =>
'form-control', 'placeholder' => 'ชื่อ-นามสกุล']);
?>
 </div>
 <div class="form-group">
 {!! Form::label('email', 'อีเมล'); !!}
{!! Form::text('email',null,['class' =>'form-control','placeholder' => 'name@gmail.com']); !!}
 </div>
 <div class="form-group">
 {!! Form::label('passowrd', 'รหัสผ่าน'); !!}
{!! Form::text('passowrd',null,['class' =>'form-control','placeholder' => 'รหัสผ่าน']); !!}
 </div>
 
  <div class="form-group">
 <?= Form::label('address', 'ที่อยู่');
?>
 <?= Form::text('address', null, ['class' =>
'form-control', 'placeholder' => 'ที่อยู่']);
?>
 </div>
 <div class="form-group">
 <?= Form::label('tel', 'โทรศัทพ์');
?>
 <?= Form::text('tel', null, ['class' =>
'form-control', 'placeholder' => 'โทรศัทพ์']);
?>
 </div>
 <div class="form-group">
 <?= Form::label('staff_id', 'รหัสพนักงาน');
?>
 <?= Form::text('staff_id', null, ['class' =>
'form-control', 'placeholder' => 'รหัสพนักงาน']);
?>
 </div>
       <div class="form-group">
 <?= Form::label('stock_id', 'รหัสสต็อก');
?>
 <?= Form::text('stock_id', null, ['class' =>
'form-control', 'placeholder' => 'รหัสสต็อก']);
?>
 </div>
     <div class="form-group">
 <?= Form::label('status', 'สถานนะ');
?>
 <?= Form::text('status', null, ['class' =>
'form-control', 'placeholder' => 'สถานนะ']);
?>
 </div>
 <div class="form-group">
 <?= Form::submit('เพิ่มสมาชิก', ['class' => 'btn btn-primary']); ?>
 </div>
 {!! Form::close() !!}
 </div>
 </div>
 </div>
 </div>
</div>
@endsection