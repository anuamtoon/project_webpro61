@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row">
 <div class="col-lg-10 col-log-offset-1">
 <div class="card mt-3">
 <div class="card-header h3">
 เพิมสต็อก
 </div>
     
     
 @if (count($errors) > 0)
 <div class="alert alert-warning">
 <ul>
 @foreach ($errors->all() as $error)
 <li>{{ $error }}</li>
 @endforeach
 </ul>
 </div>
 @endif
 
 
 <div class="card-body">
 {!! Form::open(array('url' => 'stocks','files' =>
true)) !!}

 <div class="form-group">
 <?= Form::label('fullname', 'ชื่อ-นามสกุล');
?>
 <?= Form::text('fullname', null, ['class' =>
'form-control', 'placeholder' => 'ชื่อ-นามสกุล']);
?>
 </div>
  <div class="form-group">
 <?= Form::label('address', 'ที่อยู่');
?>
 <?= Form::text('address', null, ['class' =>
'form-control', 'placeholder' => 'ที่อยู่']);
?>
 </div>
 <div class="form-group">
 <?= Form::label('tel', 'โทรศัทพ์');
?>
 <?= Form::text('tel', null, ['class' =>
'form-control', 'placeholder' => 'โทรศัทพ์']);
?>
 </div>
<div class="form-group">
 <?= Form::label('product_id', 'รหัสสินค้า');
?>
 <?= Form::text('product_id', null, ['class' =>
'form-control', 'placeholder' => 'รหัสสินค้า']);
?>
 </div>
 <div class="form-group">
 <?= Form::submit('แก้ไขข้อมูล', ['class' => 'btn btn-primary']); ?>
 </div>
 {!! Form::close() !!}
 </div>
 </div>
 </div>
 </div>
</div>
@endsection