@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row">
 <div class="col-lg-10 col-log-offset-1">
 <div class="card mt-3">
 <div class="card-header h3">
 แกไขข้อมูล {{ $product->fullname }}
 </div>
 <div class="card-body">
 @if (count($errors) > 0)
<div class="alert alert-warning">
 <ul>
 @foreach ($errors->all() as $error)
<li>{{ $error }}</li>
 @endforeach
 </ul>
 </div>
@endif
 <?= Form::model($product, array('url' => 'products/' . $product->id,
'method' => 'put','files' => true)) ?>
<div class="form-group">
 <?= Form::label('fullname', 'ชือสินค้า');
?>
 <?= Form::text('fullname', isset($product->fullname)? $product->fullname:null, ['class' =>
'form-control', 'placeholder' => 'ชือสินค้า']);
?>
 </div>
 <div class="form-group">
 <?= Form::label('info', 'รายละเอียดสินค้า');
?>
 <?= Form::text('info', isset($product->info)? $product->info:null, ['class' =>
'form-control', 'placeholder' => 'รายละเอียดสินค้า']);
?>
 </div>
 <div class="form-group">
 {!! Form::label('price', 'ราคา'); !!}
{!! Form::text('price',isset($product->price)? $product->price:null,['class' =>'form-control','placeholder' => 'เช่น 100, 100.25']); !!}
 </div>
 <div class="form-group">
 <?= Form::label('type', 'ชนิดสินค้า');
?>
 <?= Form::text('type',isset($product->type)? $product->type:null, ['class' =>
'form-control', 'placeholder' => 'ชนิดสินค้า']);
?>
 </div>
<div>
 <a href="{{ asset('images/'.$product->image) }}">
        <img src="{{ asset('images/resize/'.$product->image) }}"
      style="width:100px"></a>
 </div>
 <div class="form-group">
 {!! Form::label('image', 'แก้ไขรูปภาพ'); !!}
<?= Form::file('image', null, ['class' =>
'form-control']) ?>
 </div>
 <div class="form-group">
 <?= Form::submit('บันทึก', ['class' => 'btn btn-primary']); ?>
 </div>
 {!! Form::close() !!}
 </div>
 </div>
 </div>
 </div>
</div>
@endsection