@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row">
 <div class="col-lg-10 col-log-offset-1">
 <div class="card mt-3">
 <div class="card-header h3">
 เพิมสินค้า
 </div>
     
     
 @if (count($errors) > 0)
 <div class="alert alert-warning">
 <ul>
 @foreach ($errors->all() as $error)
 <li>{{ $error }}</li>
 @endforeach
 </ul>
 </div>
 @endif
 
 
  <div class="card-body">
  {!! Form::open(array('url' => 'products','files' =>
true)) !!}
<div class="form-group">
 <?= Form::label('fullname', 'ชื่อสินค้า');
?>
 <?= Form::text('fullname', null, ['class' =>
'form-control', 'placeholder' => 'ชื่อสินค้า']);
?>
 </div>
 <div class="form-group">
 <?= Form::label('info', 'รายละเอียด');
?>
 <?= Form::text('info', null, ['class' =>
'form-control', 'placeholder' => 'รายละเอียด']);
?>
 </div>
  <div class="form-group">
 <?= Form::label('price', 'ราคา');
?>
 <?= Form::text('price', null, ['class' =>
'form-control', 'placeholder' => 'ราคา']);
?>
 </div>
 <div class="form-group">
 <?= Form::label('type', 'ชนิดสินค้า');
?>
 <?= Form::text('type', null, ['class' =>
'form-control', 'placeholder' => 'ชนิดสินค้า']);
?>
 </div>
<div class="form-group">
 {!! Form::label('image', 'เพิ่มรูปภาพ'); !!}
<?= Form::file('image', null, ['class' =>
'form-control']) ?>
 </div>
 <div class="form-group">
 <?= Form::submit('เพิ่มข้อมูล', ['class' => 'btn btn-primary']); ?>
 </div>
 {!! Form::close() !!}
 </div>
 </div>
 </div>
 </div>
</div>
@endsection