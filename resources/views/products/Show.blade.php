@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row">
 <div class="col-lg-10 col-lg-offset-1">
     
 <div class="card mt-3">

 <div class="card-body">
 <table class="table table-striped">
 <tr>    
<th>รหัส</th>
<th>ชื่อสินค้า</th>
<th>ละลายเอียด</th>
<th>ราคาสินค้า</th>
<th>ชนิดสินค้า</th>
<th>รูปภาพ</th>
</tr>

 
@foreach ($products as $product)
<tr>
 <td>{{ $product->id }}</td>
 <td>{{ $product->fullname }}</td>
  <td>{{ $product->info}}</td>
  <td>{{ number_format($product->price,2)}}</td>
 <td>{{ $product->type}}</td>

<td><a href="{{ asset('images/'.$product->image) }}"data-lity>
        <img src="{{ asset('images/resize/'.$product->image) }}"
      style="width:100px"></a></td>
 <td>
 </tr>
 @endforeach
 </table>
     <br>
        <div class="col-md-4 text-center">
    <form action={{url('search')}}>
            <input type="text" name="searchData" >
            <i class="fa fa-search"></i>
                <button type="submit" class="btn btn-primary ">Search</button>
                
            </span>
                </form>
</div>
    
 </div>
 </div>
 </div>
 </div>
</div>

@endsection

@section('footer')
@if (session()->has('status'))
<script>
 swal({
 title: "<?php echo session()->get('status'); ?>",
 text: "",
 timer: 2000,
 type: 'success',
 showConfirmButton: false
 });
</script>
@endif
@endsection