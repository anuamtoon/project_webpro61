<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ระบบตรวจสอบคลังสินค้าจากผู้ใช้หลายคน</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style type="text/css">
            html, body {
          
           
            background-image: url("images/22.jpg");
            background-repeat: no-repeat;

            background-position: center center;

            background-attachment: fixed;

            -o-background-size: 100% 100%, auto;

            -moz-background-size: 100% 100%, auto;

            -webkit-background-size: 100% 100%, auto;

           background-size: 100% 100%, auto;
           color: white;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: white;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">หน้าหลัก</a>
                    @else
                        <a href="{{ url('/login') }}">เข้าสู่ระบบ</a>
                        <a href="{{ url('/register') }}">ลงทะเบียน</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    ระบบตรวจสอบคลังสินค้าจากผู้ใช้หลายคน
                </div>
  
                

              
            </div>
        </div>
    </body>
</html>
