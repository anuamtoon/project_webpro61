@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row">
 <div class="col-lg-10 col-lg-offset-1">
 <?= link_to('staffs/create', $title = 'เพิมข้อมูล',
['class' => 'btn btn-primary'], $secure = null); ?>  
     
 <div class="card mt-3">

 <div class="card-header h3">
 แสดงข้อมูลพนักงาน [ทั้งหมด {{ $count }} รายการ]
 </div>
 <div class="card-body">
 <table class="table table-striped">
<tr>
 <th>รหัส</th>
<th>ชื่อ</th>
<th>ที่อยู่</th>
<th>เบอร์โทร</th>
<th>แก้ไข</th>
<th>ลบ</th>
</tr>

 
@foreach ($staffs as $staff)
<tr>
 <td>{{ $staff->id }}</td>
 <td>{{ $staff->fullname }}</td>
  <td>{{ $staff->address }}</td>
   <td>{{ $staff->tel }}</td>

 <td>
 <a href="{{ url('/staffs/'.$staff->id.'/edit') }}">แก้ไข</a>
 </td>
 <td>

 <?= Form::open(array('url' => 'staffs/' . $staff->id, 'method' => 'delete','onsubmit' => 'return confirm("
แน่ใจว่าต้องการลบข้อมลู?");')) ?>
 <button type="submit" class="btn btn-danger">ลบ</button>
 {!! Form::close() !!}

 </td>
 </tr>
 @endforeach
 </table>
     <br>
      {!! $staffs->render() !!} 
 </div>
 </div>
 </div>
 </div>
</div>

@endsection

@section('footer')
@if (session()->has('status'))
<script>
 swal({
 title: "<?php echo session()->get('status'); ?>",
 text: "",
 timer: 2000,
 type: 'success',
 showConfirmButton: false
 });
</script>
@endif
@endsection