@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row">
 <div class="col-lg-10 col-log-offset-1">
 <div class="card mt-3">
 <div class="card-header h3">
 แกไขข้อมูล {{ $staff-> fullname }}
 </div>
 <div class="card-body">
 @if (count($errors) > 0)
<div class="alert alert-warning">
 <ul>
 @foreach ($errors->all() as $error)
<li>{{ $error }}</li>
 @endforeach
 </ul>
 </div>
@endif
 <?= Form::model($staff, array('url' => 'staffs/' . $staff->id,
'method' => 'put','files' => true)) ?>
<div class="form-group">
 <?= Form::label('name', 'Username');
?>
 <?= Form::text('name', null, ['class' =>
'form-control', 'placeholder' => 'Username']);
?>
 </div>
<div class="form-group">
 <?= Form::label('fullname', 'ชื่อ-นามสกุล');
?>
 <?= Form::text('fullname', null, ['class' =>
'form-control', 'placeholder' => 'ชื่อ-นามสกุล']);
?>
 </div>
 <div class="form-group">
 {!! Form::label('email', 'อีเมล'); !!}
{!! Form::text('email',null,['class' =>'form-control','placeholder' => 'name@gmail.com']); !!}
 </div>
  <div class="form-group">
 <?= Form::label('address', 'ที่อยู่');
?>
 <?= Form::text('address', null, ['class' =>
'form-control', 'placeholder' => 'ที่อยู่']);
?>
 </div>
 <div class="form-group">
 <?= Form::label('tel', 'โทรศัทพ์');
?>
 <?= Form::text('tel', null, ['class' =>
'form-control', 'placeholder' => 'โทรศัทพ์']);
?>
 </div>
 <div class="form-group">
 <?= Form::submit('แก้ไขข้อมูล', ['class' => 'btn btn-primary']); ?>
 </div>
 {!! Form::close() !!}
 </div>
 </div>
 </div>
 </div>
</div>
@endsection