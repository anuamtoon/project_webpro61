<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = 'Apichet Nuamyoon';
        $user->email = 'anuamtoon@gmail.com';
        $user->password = Hash::make('123456');
        $user->save();
    }
}
