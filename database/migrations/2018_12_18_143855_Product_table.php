<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
        $table->increments('id'); 
        $table->string('fullname'); 
        $table->string('info'); 
        $table->decimal('price',10,2);
        $table->string('type'); 
        $table->string('iamge'); 
        $table->timestamps();
 }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('product');
    }
}
