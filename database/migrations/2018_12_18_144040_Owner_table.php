<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OwnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner', function (Blueprint $table) {
        $table->increments('id'); 
        $table->string('fullname'); 
        $table->string('address'); 
        $table->integer('tel'); 
        $table->string('status'); 
        $table->integer('staff_id')->unsigned();
        $table->integer('stock_id')->unsigned();
        $table->foreign('staff_id')->references('id')->on('staff');
        $table->foreign('stock_id')->references('id')->on('stock');
        $table->timestamps();
 }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner');
    }
}
