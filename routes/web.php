<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::get('about', 'SiteController@index')->name('about');
//Route::get('about', 'SiteController@index);
Route::resource('/admin', 'AdminController');
Route::resource('owners', 'OwnerController');
Route::get('search', 'ProductController@search');

//Route::resource('test', 'OwnerController@show');

Route::resource('stocks', 'StockController');
Route::resource('staffs', 'StaffController');
Route::resource('products', 'ProductController', ['names' => [
        'index' => 'products.new',
        'destroy' => 'products.destroy',
    ]]);
Route::get('/home', 'HomeController@index')->name('home');




//Route::resource('products','ProductController');

//Route::resource('products','ProductController@index')->name('products');
//Route::get('/products/destroy/{id}', 'ProductController@destroy');







Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
